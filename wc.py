import fileinput
import sys

# словарь команд { command -> lambda }
commands = {
    '-l': lambda x: x.count('\n'),
    '-w': lambda x: len(x.split(sep=' ')),
    '-c': lambda x: sys.getsizeof(x),
}

# если аргументы это файлы/stdin без ключей
if len(sys.argv) == 1 or (len(sys.argv) > 1 and sys.argv[1] not in commands):
    content = ''.join(fileinput.input(sys.argv[1:]))  # данные считанные в строку  из файлов/stdin
    sys.stdout.write(
        '{}\t{}\t{}'.format(
            commands['-l'](content),
            commands['-w'](content),
            commands['-c'](content)
        )
    )
# если аргументом является команда
elif sys.argv[1] in commands:
    sys.stdout.write(
        commands[sys.argv[1]](  # вызов анонимной функции из словаря по ключу (аргументу)
            ''.join(fileinput.input(sys.argv[2:]))  # данные считанные в строку из файлов/stdin
        )
    )
